# HO 4 – Fugazi

<img src="/splash/bosch-s1e4.png" width="25%" />

Hosts: [Fabian A. Scherschel](https://twitter.com/fabsh) and [Dan Lynch](https://twitter.com/methoddan)

You can [download the podcast at Sixgun Productions](http://sixgun.org/ho/4) and discuss the episode [via this GitLab issue](https://gitlab.com/fab.industries/ho/issues/4).

---

_As Fab has returned from his trip to Hollywood, we dive back into Bosch with what many people think is the best episode of the first season._

Fab talks a bit about his trip through Southern California and Nevada. He visited the [USS Midway museum](https://en.wikipedia.org/wiki/USS_Midway_Museum). We also mention the [Ivanpah Solar Power Facility](https://en.wikipedia.org/wiki/Ivanpah_Solar_Power_Facility) in San Diego.

<img src="http://sixgun.org/files/sites/5/midway-bridge.jpg" width="50%" />

<img src="http://sixgun.org/files/sites/5/crate-and-barrel.jpg" width="50%" />

<img src="http://sixgun.org/files/sites/5/ivanpah.jpg" width="50%" />

<img src="http://sixgun.org/files/sites/5/charger.jpg" width="50%" />

<img src="http://sixgun.org/files/sites/5/hemi.jpg" width="50%" />

<img src="http://sixgun.org/files/sites/5/mustang.jpg" width="50%" />

<img src="http://sixgun.org/files/sites/5/mulholland-drive.jpg" width="50%" />

Today's episode is _Bosch S1E4 &ndash; ["Chapter Four: Fugazi"](https://www.imdb.com/title/tt4052098/):_

> The bones investigation uncovers the family's dark past. As Bosch's court case reaches a climactic verdict, he gets caught up in a dangerous field trip with Waits, who may be setting him up. A stunning turn of events leads to a frantic citywide manhunt, and Bosch is back on the hot seat.

You can give us feedback on the show [via Twitter](https://twitter.com/hwoutlaws), [on GitLab](https://gitlab.com/fab.industries/ho/issues/) or [by joining the Sixgun Discord](http://discord.sixgun.org/) and talking to us directly.

---

Our intro music is the song _Remington Magnum Express_ by the [Bourbon Boys](https://open.spotify.com/artist/2ubeUsJWHWzu2ul4qA2jFt), courtesy of and copyright &copy; 2013 [Faravid Recordings](https://faravidrecordings.com). Bandwidth for this podcast is graciously provided by [Bytemark](https://www.bytemark.co.uk/).
