# HO 3 – Blue Religion

<img src="/splash/bosch-s1e3.png" width="25%" />

Hosts: [Fabian A. Scherschel](https://twitter.com/fabsh) and [Dan Lynch](https://twitter.com/methoddan)

You can [download the podcast at Sixgun Productions](http://sixgun.org/ho/3) and discuss the episode [via this GitLab issue](https://gitlab.com/fab.industries/ho/issues/3).

---

_In today's episode, Bosch is investigating the family of the murder victim in his cold case. We can neither confirm nor deny that it was the good looking creep with the body in the back of his van._

As part of this episode, the concept of a [Glomar Response](https://en.wikipedia.org/wiki/Glomar_response) came up, which is quite fascinating. The whole story of [Project Azorian](https://en.wikipedia.org/wiki/Project_Azorian) and Howard Hughes involvment in the search for and recovery of [K-129](https://en.wikipedia.org/wiki/Soviet_submarine_K-129_(1960)) is pretty crazy. If you are interested in all of this, you might want to listen to [this podcast](https://www.pri.org/stories/2015-09-07/ship-built-cias-most-audacious-cold-war-mission-now-headed-scrapyard).

Anyway, back to Bosch and today's episode, which is _Bosch S1E3 &ndash; ["Chapter Three: Blue Religion"](https://www.imdb.com/title/tt4071618/):_

> After the young victim in the bones case is identified, Bosch and J. Edgar are pulled into the troubled world of the boy's family. As Bosch's own trial escalates, his romance with Brasher takes a turn. And an intense confrontation with Raynard Waits forces Bosch to rethink everything.

You can give us feedback on the show [via Twitter](https://twitter.com/hwoutlaws), [on GitLab](https://gitlab.com/fab.industries/ho/issues/) or [by joining the Sixgun Discord](http://discord.sixgun.org/) and talking to us directly.

---

Our intro music is the song _Remington Magnum Express_ by the [Bourbon Boys](https://open.spotify.com/artist/2ubeUsJWHWzu2ul4qA2jFt), courtesy of and copyright &copy; 2013 [Faravid Recordings](https://faravidrecordings.com). Bandwidth for this podcast is graciously provided by [Bytemark](https://www.bytemark.co.uk/).
