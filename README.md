# Hollywood Outlaws

Show notes, discussions and issue tracking for the [_Hollywood Outlaws_](http://sixgun.org/ho) podcast.

### Episode List

* [HO 8 – High Low](/shownotes/ho-008.md)
* [HO 7 – Lost Boys](/shownotes/ho-007.md)
* [HO 6 – Donkey's Years](/shownotes/ho-006.md)
* [HO 5 – Mama's Boy](/shownotes/ho-005.md)
* [HO 4 – Fugazi](/shownotes/ho-004.md)
* [HO 3 – Blue Religion](/shownotes/ho-003.md)
* [HO 2 – Lost Light](/shownotes/ho-002.md)
* [HO 1 – 'Tis the Season](/shownotes/ho-001.md)
